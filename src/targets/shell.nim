# This file is a part of Builder.
#
# Builder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Builder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Builder.  If not, see <http://www.gnu.org/licenses/>.

import os
import osproc

import parsetoml

import ../consoleoutput
import common


proc build*(task: TomlTableRef, dryrun: bool): BuildResult =
  var
    command: string
    expected = 0
    args: seq[string]

  if task.hasKey("expected"):
    if task["expected"].kind != TomlValueKind.Int:
      message(MsgError, "Malformed key 'expected' should be an int")
      return BuildResult.Malformed
    else:
      expected = task["expected"].getInt

  if task.hasKey("command"):
    if task["command"].kind != TomlValueKind.String:
      message(MsgError, "Malformed key 'command' should be an int")
      return BuildResult.Malformed
    else:
      command = $task["command"]
  else:
    message(MsgError, "Missing required key 'command'")
    return BuildResult.Malformed

  if task.hasKey("args"):
    if task["args"].kind != TomlValueKind.Array:
      message(MsgError, "Malformed key 'args' should be an array of strings")
      return BuildResult.Malformed
    else:
      for arg in task["args"].getElems:
        if arg.kind != TomlValueKind.String:
          message(MsgError, "Malformed item in array key 'args' should be a string")
          return BuildResult.Malformed
        else:
          args.add($arg)

  message(MsgInfo, "Executing commandline: ", MsgTaskNameError, $task["command"])

  if dryRun:
    message(MsgInfo, "Exiting task due to dryrun")
    return BuildResult.Success

  if command == nil:
    echo "Command is nil!"
  if args == nil:
    echo "Args are nil!"

  let externalCommand = startProcess(command, nil, args, nil, {poStdErrToStdOut, poUsePath, poParentStreams})
  defer: externalCommand.close()

  if externalCommand == nil:
    echo "External command is somehow nil!?"

  let status = externalCommand.waitForExit()

  if status == expected:
    return BuildResult.Success
  else:
    message(MsgError, "Command exited with status: ", MsgStatus, $status)
    return BuildResult.Failure
