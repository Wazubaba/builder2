# This file is a part of Builder.
#
# Builder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Builder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Builder.  If not, see <http://www.gnu.org/licenses/>.

import os
import osproc
import strutils

import parsetoml

import ../project
import ../consoleoutput
import ../version
import common


proc build*(task: TomlTableRef, dryrun: bool): BuildResult =
  var commandline: string

  # Attempt to read information from the task
  var nimMinimum = ""
  if task.hasKey("nim"):
    if task["nim"].kind == TomlValueKind.String:
      nimMinimum = $task["nim"]
    else:
      message(MsgError, "Malformed key 'nim' should be a string!")
      return BuildResult.Malformed

  var cache = ""
  if task.hasKey("cache"):
    if task["cache"].kind == TomlValueKind.String:
      cache = "--nimcache:" & $task["cache"]
      message(MsgInfo, "Using cache: ", MsgFileName, $task["cache"])
    else:
      message(MsgError, "Malformed key 'cache' should be a string!")
      return BuildResult.Malformed

  var outpath = ""
  if task.hasKey("outpath"):
    if task["outpath"].kind == TomlValueKind.String:
      outpath = "-o:" & unixToNativePath($task["outpath"])
    else:
      message(MsgError, "Malformed key 'path' should be a string!")
      return BuildResult.Malformed

  var sources: string
  if task.hasKey("sources"):
    if task["sources"].kind == TomlValueKind.Array:
      for file in task["sources"].getElems:
        sources.add(" " & $file)
      sources = sources.strip()
    else:
      message(MsgError, "Malformed key 'sources' should be an array of strings!")
      return BuildResult.Malformed
  else:
    sources = "src/main.nim"
    message(MsgInfo, "Using default main file: ", MsgFileName, sources)

  var generatorBuffer: string
  var generator: string = "c"
  if task.hasKey("generator"):
    if task["generator"].kind == TomlValueKind.String:
      generatorBuffer = $task["generator"]
      case generatorBuffer.toLower()
      of "c": generator = "c"
      of "cpp": generator = "cpp"
      of "objc": generator = "objc"
      of "js": generator = "js"
      else:
        message(MsgError, "Unknown generator: ", MsgInfo, generatorBuffer)
        return BuildResult.Malformed
    else:
      message(MsgError, "Malformed key 'generator' should be a string!")
      return BuildResult.Malformed

  var buildTypeBuffer: string
  var buildType = ""
  if task.hasKey("buildtype"):
    if task["buildtype"].kind == TomlValueKind.String:
      buildTypeBuffer = $task["buildtype"]
      case buildTypeBuffer.toLower()
      of "binary": buildType = ""
      of "console": buildType = "--app:console"
      of "gui": buildType = "--app:gui"
      of "lib": buildType = "--app:lib"
      of "staticlib": buildType = "--app:staticlib"
      else:
        message(MsgError, "Unknown build type: ", MsgInfo, buildTypeBuffer)
        return BuildResult.Malformed
    else:
      message(MsgError, "Malformed key 'buildtype' should be a string!")
      return BuildResult.Malformed

  var arguments = ""
  if task.hasKey("arguments"):
    if task["arguments"].kind == TomlValueKind.Array:
      for arg in task["arguments"].getElems:
        arguments.add(" " & $arg)
      arguments = arguments.strip()
    else:
      message(MsgError, "Malformed key 'arguments' should be an array of strings!")
      return BuildResult.Malformed

  var includes = ""
  if task.hasKey("includes"):
    if task["includes"].kind == TomlValueKind.Array:
      for arg in task["includes"].getElems:
        includes.add(" -p:" & $arg)
      includes = includes.strip()

  var version = get_nim_version()

  if version.invalid:
    return BuildResult.Malformed


  # Test if the version of nim is >= the optional required minimum version
  var reqMinNim = nimMinimum.parse_version_number()
  if reqMinNim == (-1, -1, -1) and nimMinimum != "":
    message(MsgError, "Failed to parse minimum required version!")
    return BuildResult.Malformed
  else:
    if version.version_number_lt(reqMinNim):
      message(MsgError, "Build aborted! ", MsgInfo, "Your version of Nim is too old: ", MsgVersion, reqMinNim.version2String())
      return BuildResult.Malformed

  # Now construct the commandline
  commandline = "nim"
  commandline.add(" " & generator)

  if arguments != "": commandline.add(" " & arguments)
  if outpath != "": commandline.add(" " & outpath)
  if includes != "": commandline.add(" " & includes)
  if buildType != "": commandline.add(" " & buildType)
  if cache != "": commandline.add(" " & cache)

  commandline.add(" " & sources)

#  message(MsgInfo, "Using Nim version ", MsgVersion, version.version2String())
  message(MsgInfo, "Building with commandline ", MsgCommandline, commandline)

  if dryrun:
    message(MsgInfo, "Exiting task due to dryrun")
    return BuildResult.Success

  return if execShellCmd(commandline) == 0: BuildResult.Success
  else: BuildResult.Failure
