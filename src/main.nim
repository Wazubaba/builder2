# This file is a part of Builder.
#
# Builder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Builder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Builder.  If not, see <http://www.gnu.org/licenses/>.

import parseopt

import project
import driver


let VERSION = "build 1.5.0 compiled with Nim: " & NimVersion

var
  dryrun = false
  targetFile = "project.toml"
  tasks: seq[string]
  infoMode = false
  listMode = false
  allMode = false

proc showHelp() =
  echo """
Usage: builder [options] [task1] [task2] [task3]...

Options:
    -h, --help                Show this help and exit
    -v, --version             Show version and exit
    --dryrun                  Parse the build file but don't actually
                                execute any tasks
    -p, --project-file=path   Specify project file to use (defaults to
                                project.ini)
    -i, --info                Show task information and exit
    -l, --list                List defined tasks and exit
    -a, --all                 Instruct info mode to show all tasks
"""

for kind, key, val in getOpt():
  case kind
  of cmdLongOption, cmdShortOption:
    case key
    of "help", "h":
      showHelp()
      quit()
    of "version", "v":
      echo VERSION
      quit()
    of "dryrun":
      dryrun = true
    of "project-file", "p":
      targetFile = val
    of "info", "i":
      infoMode = true
    of "list", "l":
      listMode = true
    of "all", "a":
      allMode = true
    else:
      discard
  else:
#    if endOfOpts:
    tasks.add(key)

if tasks.len() == 0:
  tasks = @["defaults"]

var projectData = add_project_file(targetFile, listMode)

if listMode:
  quit(0)

projectData.process(tasks, dryrun, infoMode, allMode)
