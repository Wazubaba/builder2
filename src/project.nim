# This file is a part of Builder.
#
# Builder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Builder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Builder.  If not, see <http://www.gnu.org/licenses/>.

import tables

import parsetoml

import consoleoutput

type
  Project* = OrderedTableRef[string, TomlTableRef]

proc add_project_file*(path: string, listMode: bool): Project =
  result = newOrderedTable[string, TomlTableRef]()
  try:
    let data = parseFile(path)
    assert (data.kind == TomlValueKind.Table)

    if listMode:
      message(MsgInfo, "Available tasks:")

    for task in data.getTable().keys:

      if data[task].kind != TomlValueKind.Table:
        message(MsgWarning, "Skipping unbound value: ", MsgTaskNameError, task)
        continue

      elif result.hasKey(task):
        message(MsgWarning, "Skipping duplicate task: ", MsgTaskNameError, task)
        continue

      # Skip testing if defaults task has a type key as it will ALWAYS be a group type task
      elif not data[task].hasKey("type") and task != "defaults":
        message(MsgWarning, "Skipping malformed task: ", MsgTaskNameError, task)

      if listMode:
        message("\t", MsgTaskName, $task)

      result[task] = data[task].getTable()

  except IOError:
    message(MsgError, "Cannot load project file: ", MsgFileName, path)
    quit(1)

  except TomlError:
    let loc = (ref TomlError)(getCurrentException()).location
    message(MsgError, "Failed to parse project file: ", MsgFileName, path)
    echo loc.fileName & ":" & $loc.line & ":" & $loc.column & ": " & getCurrentExceptionMsg()
    quit(1)

  if listMode: return
  message(MsgInfo, "Using project file: ", MsgStatus, path)
