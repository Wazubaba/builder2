# This file is a part of Builder.
#
# Builder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Builder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Builder.  If not, see <http://www.gnu.org/licenses/>.

import terminal
import macros

type
  MsgType* = enum
    MsgInfo,
    MsgWarning,
    MsgImportant,
    MsgStatus,
    MsgSuccess,
    MsgError,
    MsgTaskName,
    MsgTaskNameError,
    MsgTargetName,
    MsgFileName,
    MsgVersion,
    MsgCommandline,
    MsgAuthorName,
    MsgDescription

# Helpers for processing messages
template taskMessageProcessArg(s: string) = write stdout, s
template taskMessageProcessArg(class: MsgType) =
  setStyle({styleBright})
  case class
  of MsgInfo: setForegroundColor stdout, fgWhite
  of MsgWarning: setForegroundColor stdout, fgYellow
  of MsgImportant: setForegroundColor stdout, fgMagenta
  of MsgStatus: setForegroundColor stdout, fgCyan
  of MsgSuccess: setForegroundColor stdout, fgGreen
  of MsgError: setForegroundColor stdout, fgRed
  of MsgTaskName: setForegroundColor stdout, fgCyan
  of MsgTaskNameError: setForegroundColor stdout, fgYellow
  of MsgTargetName: setForegroundColor stdout, fgYellow
  of MsgFileName: setForegroundColor stdout, fgMagenta
  of MsgVersion: setForegroundColor stdout, fgYellow
  of MsgCommandline: setForegroundColor stdout, fgYellow
  of MsgAuthorName: setForegroundColor stdout, fgYellow
  of MsgDescription: resetAttributes stdout

# I made this its own function because idk how to pass stdout from the macro yet
proc reset() = resetAttributes(stdout)

macro message*(args: varargs[typed]): untyped =
  result = newNimNode(nnkStmtList, args)
  for arg in args:
    result.add(newCall(bindSym"taskMessageProcessArg", arg))
  result.add(newCall(bindSym"reset"))
  result.add(newCall(bindSym"echo", newStrLitNode("")))
