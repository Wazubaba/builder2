import parsetoml

import consoleoutput

proc type_helper(valuetype: TomlValueKind): string {.inline.} =
  case valuetype
  of TomlValueKind.None: return "nil"
  of TomlValueKind.Int: return "int"
  of TomlValueKind.Float: return "float"
  of TomlValueKind.Bool: return "bool"
  of TomlValueKind.Datetime: return "datetime"
  of TomlValueKind.String: return "string"
  of TomlValueKind.Array: return "array"
  of TomlValueKind.Table: return "table"

## This is what should normally be used
proc match*(container: TomlTableRef, keyname: string, valuetype: TomlValueKind, optional: bool = true): bool {.inline.} =
  let typeString = type_helper(valuetype)

  if container.hasKey(keyname):
    if container[keyname].kind != valuetype:
      message(MsgWarning, "Malformed key '", MsgInfo, keyname, MsgWarning, "' should be a ", typeString)
      return false

    return true

  if not optional:
    message(MsgWarning, "Missing required key '", MsgInfo, keyname, MsgWarning, "' should be defined as ", typeString)

  return false


## This is specifically for subvalues in TomlArray, like when processing argument lists
proc match*(value: TomlValueRef, keyname: string, valuetype: TomlValueKind): bool {.inline.} =

  let typeString = type_helper(valuetype)

  if value.kind != valuetype:
    message(MsgWarning, "Malformed key '", MsgInfo, keyname, MsgWarning, "' should be a ", typeString)
    return false

  return true


proc is_group*(task: TomlTableRef): bool {.inline.} =
  if not task.match("type", TomlValueKind.String, false):
    return false
  else:
    if $task["type"] == "group": return true
    else: return false

