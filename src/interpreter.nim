# This file is a part of Builder.
#
# Builder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Builder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Builder.  If not, see <http://www.gnu.org/licenses/>.

import strutils
import sequtils

import parsetoml

import consoleoutput
import project
import parseutil

type DependencyScanException = object of Exception

# Prototype for scan_groups's benefit
proc scan_dependencies*(project: Project, task: string, override_first_task: bool = false): seq[string]


proc scan_groups*(project: Project, taskList: var seq[string]) =
  proc recursion_scan(project: Project, task: string, seenList: var seq[string]): seq[string] =
    if task in seenList:
      return

    if not project.hasKey(task):
      message(MsgWarning, task, MsgTaskNameError, " is not defined! Skipping...")
      return

    if project[task].match("dependencies", TomlValueKind.Array):
      result = concat(result, scan_dependencies(project, task, true))

    if not project[task].match("tasks", TomlValueKind.Array): return

    echo ""
    for subtask in project[task]["tasks"].getElems:
      seenList.add($subtask)

      if not project.hasKey($subtask):
        message(MsgWarning, $subtask, MsgTaskNameError, " is not defined! Skipping...")
        continue

      let subtaskDefinition = project[$subtask]

      if subtaskDefinition.is_group():
        message(MsgInfo, "Processing group task: ", MsgTaskName, $subtask)
        seenList.add($subtask)
        result = concat(result, recursion_scan(project, $subtask, seenList))
      else:
#        if task.toLower() == "defaults":
#          seenList.add($subtask)
#          result = concat(result, recursion_scan(project, $subtask, seenList))
#        else:
        result.add($subtask)
        seenList.add($subtask)

  var seenList: seq[string]
  for task in taskList:
    if task in seenList: continue
    if project.hasKey(task) and task != "defaults" and not project[task].is_group():
      return
    taskList = recursion_scan(project, task, seenList)

    # We've established that all groups passed are expanded - go through and cull
    # any of the group tasks passed
    let temp = taskList
    var prunedList: seq[string]
    for item in temp:
      if not project[item].is_group():
        prunedList.add(item)
    
    taskList = prunedList


proc scan_dependencies*(project: Project, task: string, override_first_task: bool = false): seq[string] =
  ## For the given task process any dependencies and build a sequence of them to return

  proc recursive_scan(project: Project, task: string, seen: var seq[string], override_first_task: bool = false): seq[string] =
    if task in seen:
      return

    seen.add(task)

    var todo: seq[string] = @[task]

    if project[task].is_group() and not override_first_task:
      scan_groups(project, todo)

    for item in todo:
      if project[item].match("dependencies", TomlValueKind.Array):
        let deplist = project[item]["dependencies"]

        if deplist.kind != TomlValueKind.Array:
          message(MsgError, "Malformed dependencies array for task: ", MsgTaskNameError, item)
          raise newException(DependencyScanException, "Malformed dependencies array for task: " & item)

        for dependency in project[item]["dependencies"].getElems:
          result = concat(result, recursive_scan(project, $dependency, seen))

      result.add(item)

  # Used to ensure if a circular dependency is found then it is able to be escaped.
  var seen: seq[string]

  try:
    result = recursive_scan(project, task, seen, override_first_task)
  except DependencyScanException:
    message(MsgError, "Aborting build")
    quit(3)
