# This file is a part of Builder.
#
# Builder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Builder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Builder.  If not, see <http://www.gnu.org/licenses/>.

#[
  This module will return the proper configuration settings for
  a given language, or raise an error if the target langauge is
  not supported. It also will contain various utility functions
  for handling various target types.
]#

import targets.nim
import targets.shell
import targets.common
export nim
export shell
export common


#[
  Loads the first accessible build definitions file and
  returns the configuration needed.
]#
#proc get_build_settings*(language: string): BuildSettings =
#  return
