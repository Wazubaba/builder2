# This file is a part of Builder.
#
# Builder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Builder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Builder.  If not, see <http://www.gnu.org/licenses/>.

#[
  Contains numerous routines to try to assist with version numbers.
  It assumes semantic version numbers but tries to work with things.
]#

import strutils
import osproc
import streams

import consoleoutput

type VersionNumber* = tuple
  major: int
  minor: int
  patch: int

proc parse_version_number*(buffer: string): VersionNumber =
  let data = buffer.split(".")
  if data.len() < 3: return (-1, -1, -1)

  var major, minor, patch: int
  try: major = data[0].parseInt()
  except ValueError: major = -1

  try: minor = data[1].parseInt()
  except ValueError: minor = -1

  try: patch = data[2].parseInt()
  except ValueError: patch = -1

  return (major, minor, patch)


proc invalid*(v1: VersionNumber): bool =
  return v1 == (-1, -1, -1)

proc version_number_lt*(v1: VersionNumber, v2: VersionNumber): bool =
  if v1.major < v2.major: return true
  if (v1.major == v2.major) and (v1.minor < v2.minor): return true
  if (v1.major == v2.major) and (v1.minor == v2.minor) and (v1.patch < v2.patch): return true
  return false

proc version2String*(v1: VersionNumber): string =
  return $v1.major & "." & $v1.minor & "." & $v1.patch


proc get_nim_version*(): VersionNumber =
  # Ensure an error version is returned if all else fails
  result = (-1, -1, -1)

  var buffer: string

  try:
    let nim = startProcess("nim", nil, ["--version"], nil, {poStdErrToStdOut, poUsePath})
    defer: nim.close()

    let nimout = nim.outputStream()


    if nimout.readLine(buffer):
      result = buffer.split()[3].parse_version_number()

    message(MsgInfo, "Found Nim version: ", MsgVersion, result.version2String())

  except OSError:
    message(MsgError, "Failed to locate nim compiler. Add it to your path and try again")
    echo getCurrentExceptionMsg()
