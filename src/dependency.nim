# This file is a part of Builder.
#
# Builder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Builder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Builder.  If not, see <http://www.gnu.org/licenses/>.

import sequtils

import parsetoml

import project
import consoleoutput

import targets.group
import parseutil

type DependencyScanException = object of Exception


proc scan_dependencies*(project: Project, task: string): seq[string] =
  ## For the given task process any dependencies and build a sequence of them to return

  proc recursive_scan(project: Project, task: string, seen: var seq[string]): seq[string] =
    if task in seen:
      return

    seen.add(task)

    var todo: seq[string] = @[task]

    if project[task].is_group():
      scan_groups(project, todo)

    for item in todo:
      if project[item].hasKey("dependencies"):
        let deplist = project[item]["dependencies"]

        if deplist.kind != TomlValueKind.Array:
          message(MsgError, "Malformed dependencies array for task: ", MsgTaskNameError, item)
          raise newException(DependencyScanException, "Malformed dependencies array for task: " & item)

        for dependency in project[item]["dependencies"].getElems:
          result = concat(result, recursive_scan(project, $dependency, seen))

      result.add(item)

  # Used to ensure if a circular dependency is found then it is able to be escaped.
  var seen: seq[string]

  try:
    result = recursive_scan(project, task, seen)
  except DependencyScanException:
    message(MsgError, "Aborting build")
    quit(3)

#  echo "DEBUG: All depedencies found for task " & task & ":"
#  for dep in result:
#    echo "\t" & dep
