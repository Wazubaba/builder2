# This file is a part of Builder.
#
# Builder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Builder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Builder.  If not, see <http://www.gnu.org/licenses/>.

import sequtils
import strutils

import parsetoml

import project
import consoleoutput
import targets
import interpreter

# Function template
proc showTaskInfo*(project: Project, tasks: seq[string], allMode: bool)

proc order_dependencies(project: Project, tasks: var seq[string]) =
  ## Sort the task build order so that any dependent tasks are built first
  message(MsgInfo, "Calculating dependencies...")
  var finalTaskList: seq[string]
  for task in tasks:
    finalTaskList = concat(finalTaskList, scan_dependencies(project, task))

  tasks = finalTaskList.deduplicate()

proc scan_subtasks(project: Project, tasks: var seq[string]) =

  ## Recursively scan a subtask chain to expand any groups found
  message(MsgInfo, "Resolving group tasks...")
  project.scan_groups(tasks)

proc resolve_task_targets(project: Project, tasks: seq[string], dryrun: bool) =
  ## Iterate the tasks and pass them to the proper target backend
  message(MsgInfo, "Building project...")
  for task in tasks:
    message(MsgTaskName, task, MsgInfo, ": ", MsgTargetName, $project[task]["type"])

    case project[task]["type"].getStr.toLower()
    of "nim":
      case targets.nim.build(project[task], dryrun)
      of BuildResult.Success:
        message(MsgSuccess, "Successfully built task: ", MsgTaskName, task)
      of BuildResult.Failure:
        message(MsgError, "Failed to build task: ", MsgTaskNameError, task)
      of BuildResult.Malformed:
        message(MsgError, "Aborting build task: ", MsgTaskNameError, task)
      of BuildResult.TargetSpecific:
        message(MsgWarning, "Target specific fail: ", MsgTaskNameError, task)

    of "shell":
      case targets.shell.build(project[task], dryrun)
      of BuildResult.Success:
        message(MsgSuccess, "Executed ", MsgTaskName, task, MsgSuccess, " successfully")
      of BuildResult.Failure:
        message(MsgError, "Failed to execute command: ", MsgTaskNameError, task)
      of BuildResult.Malformed:
        message(MsgError, "Aborting shell task: ", MsgTaskNameError, task)
      of BuildResult.TargetSpecific:
        message(MsgWarning, "Target specific fail: ", MsgTaskNameError, task)

    else:
      message(MsgWarning, "Unknown target for task: ", MsgTaskNameError, task)
      message(MsgWarning, "Skipping...")


proc process*(project: Project, tasks: var seq[string], dryrun: bool, infoMode: bool, allMode: bool) =
  ## Process the provided task(s)

  scan_subtasks(project, tasks)
  order_dependencies(project, tasks)
  if tasks.len == 0:
    message(MsgError, "After scan 0 tasks have been found")
    quit(2)

  echo ""

  if infoMode:
    project.showTaskInfo(tasks, allMode)
    quit(0)
  else:
    message(MsgInfo, "Scan complete, tasks to be built:")
    for task in tasks:
        message(MsgTaskName, task)

    echo ""
    resolve_task_targets(project, tasks, dryrun)


proc showTaskInfo*(project: Project, tasks: seq[string], allMode: bool) =
  var toShow: seq[string]
  if allMode:
    for task in project.keys:
      toShow.add(task)
  else:
    toShow = tasks

  for task in toShow:
    if not project.hasKey(task):
      message(MsgWarning, "Task ", MsgTaskName, task, MsgInfo, " is not defined, skipping!")
      continue
    let taskInfo = project[task]

    var
      name = task
      author: string
      description: string

    if taskInfo.hasKey("name"):
      if taskInfo["name"].kind != TomlValueKind.String:
        message(MsgWarning, "Task ", MsgTaskNameError, task, MsgInfo, ": ", MsgWarning, "Malformed key 'name' should be a string, skipping!")
        continue
      else:
        name = $taskInfo["name"]

    if taskInfo.hasKey("description"):
      if taskInfo["description"].kind != TomlValueKind.String:
        message(MsgWarning, "Task ", MsgTaskNameError, task, MsgInfo, ": ", MsgWarning, "Malformed key 'description' should be a string, skipping!")
        continue
      else:
        description = $taskInfo["description"]

    if taskInfo.hasKey("author"):
      if taskInfo["author"].kind != TomlValueKind.String:
        message(MsgWarning, "Task ", MsgTaskNameError, task, MsgInfo, ": ", MsgWarning, "Malformed key 'author' should be a string, skipping!")
        continue
      else:
        author = $taskInfo["author"]

    message(MsgInfo, "[", MsgTaskName, name, MsgInfo, "]")
    if author != "":
      message(MsgInfo, "Author: ", MsgAuthorName, author)
    if description != "":
      message(MsgInfo, "Description: ")
      message(MsgDescription, description)

    echo ""
