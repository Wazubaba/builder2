#!/bin/bash

# This file is a part of Builder.
#
# Builder is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Builder is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Builder.  If not, see <http://www.gnu.org/licenses/>.

function show_help {
	/bin/echo "Usage: ./bootstrap.sh [OPTIONS]"
	/bin/echo ""
	/bin/echo "Options:"
	/bin/echo -e  "\tstrip    -  strip resulting binary"
	/bin/echo -e  "\tupx      -  try to run upx --best --ultra-brute on the binary"
#	/bin/echo -e  "\trelease  -  build in release mode"
}



echo "***Builder bootstrap script***"

DEBUG=1
STRIP=0
UPX=0

NIM=`which nim`
VERSION=`nim --version | sed 's/^.*[^0-9]\([0-9]*\.[0-9]*\.[0-9]*\).*$/\1/' | head -n1`

IFS='.' read -a ARR <<< "$VERSION"

VERSION_MAJOR=${ARR[0]}
VERSION_MINOR=${ARR[1]}
VERSION_PATCH=${ARR[2]}

echo "Found nim: $NIM"
echo "Version: $VERSION_MAJOR.$VERSION_MINOR.$VERSION_PATCH"
echo ""

if [ $VERSION_MAJOR -lt 0 ] || [ $VERSION_MINOR -lt 18 ] || [ $VERSION_PATCH -lt 1 ]; then
	echo "Your version of nim is too old to build this."
	echo "Minimum version required is 0.18.1"
	exit 1
fi

if [ $# -gt 0 ]; then
	for arg in "$@"; do
		case $arg in
			"--help"|"-h")
				show_help
				exit 0
				;;
#			"release")
#				DEBUG=0
#				;;
			"strip")
				STRIP=1
				;;
			"upx")
				UPX=1
				;;
			*)
			echo "Invalid option: $arg"
			exit 1
			;;
		esac
	done
fi

OUT='--out:build'
CACHE='--nimcache:.nimcache'
WARNINGS='--hints:on --warnings:on'

OPTS="$CACHE $WARNINGS $OUT --path:submodules/parsetoml/src"

if [ $DEBUG -eq 1 ]; then
	echo "Building in debug mode"
else
	echo "Building in release mode"
	OPTS="$OPTS -d:release"
fi

$NIM c $OPTS src/main.nim

if [ $STRIP -eq 1 ]; then
	echo "Stripping executable"
	strip --strip-unneeded build
fi

if [ $UPX -eq 1 ]; then
	if which upx; then
		echo "Compressing with `which upx`..."
		upx --best --ultra-brute build
	else
		echo "No UPX binary found!"
	fi
fi
